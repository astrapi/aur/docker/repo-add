#! /bin/bash

if test -z "$1";
then
	echo "ERROR: pls specify a software to build!"
	exit 1
fi

ls -la /repo
repo-add /repo/$1.db.tar.gz /repo/*.pkg.tar.*
