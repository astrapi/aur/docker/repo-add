ARG ARCH=
FROM archlinux:$ARCH

RUN pacman -Syu --needed --noconfirm shadow sudo

COPY run.sh /usr/bin
RUN chmod +x /usr/bin/run.sh

# makepkg user and workdir
ARG user=makepkg
RUN useradd --system --create-home $user \
  && echo "$user ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/$user
USER $user

ENTRYPOINT ["run.sh"]
